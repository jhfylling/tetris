package johan.tetris.Tetrominoes;

import android.graphics.Color;

import johan.tetris.GameBoard;
import johan.tetris.Tetromino;

public class O extends Tetromino {

    private int[][][] orientationOffsets =
            {       {{1, 1}, {2, 1}, {2, 2}, {1, 2}},
                    {{1, 1}, {2, 1}, {2, 2}, {1, 2}},
                    {{1, 1}, {2, 1}, {2, 2}, {1, 2}},
                    {{1, 1}, {2, 1}, {2, 2}, {1, 2}}
            };

    private GameBoard gameBoard;

    public O(int id, int orientation, int[] position, GameBoard gameBoard) {
        super(id, TetrominoType.O, orientation, position, gameBoard);

        this.gameBoard = gameBoard;
        this.setTileOffsets(orientationOffsets[orientation]);
    }

    @Override
    public int getColor() {
        return Color.rgb(
                242, 242, 24
        );
    }

    @Override
    public void rotate() {
        this.setTileOffsets(orientationOffsets[(this.getOrientation() + 1) % 4]);

        for(int[] orientation : orientationOffsets[this.getOrientation()]){
            gameBoard.clearTile(orientation[0] + this.getPosition()[0], orientation[1] + this.getPosition()[1]);
        }

        this.setOrientation(this.getOrientation() + 1);

    }
}
