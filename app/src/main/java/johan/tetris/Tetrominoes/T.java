package johan.tetris.Tetrominoes;

import android.graphics.Color;

import johan.tetris.GameBoard;
import johan.tetris.Tetromino;

public class T extends Tetromino {

    private int[][][] orientationOffsets =
            {       {{1, 0}, {0, 1}, {1, 1}, {2, 1}},
                    {{0, 0}, {0, 1}, {0, 2}, {1, 1}},
                    {{0, 1}, {1, 1}, {2, 1}, {1, 2}},
                    {{1, 0}, {1, 1}, {1, 2}, {0, 1}}
            };

    private GameBoard gameBoard;

    public T(int id, int orientation, int[] position, GameBoard gameBoard) {
        super(id, TetrominoType.T, orientation, position, gameBoard);

        this.gameBoard = gameBoard;
        this.setTileOffsets(orientationOffsets[orientation]);
    }

    @Override
    public int getColor() {
        return Color.rgb(101, 186, 93);
    }

    @Override
    public void rotate() {
        this.setTileOffsets(orientationOffsets[(this.getOrientation() + 1) % 4]);

        for(int[] orientation : orientationOffsets[this.getOrientation()]){
            gameBoard.clearTile(orientation[0] + this.getPosition()[0], orientation[1] + this.getPosition()[1]);
        }

        this.setOrientation(this.getOrientation() + 1);

    }
}
