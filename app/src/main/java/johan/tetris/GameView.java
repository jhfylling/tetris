package johan.tetris;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    public final static int GRIDWIDTH = 10;
    public final static int GRIDHEIGHT = 20;

    private GameThread gameThread = null;
    private GameBoard gameBoard;

    private int canvasWidth;
    private int canvasHeight;

    @SuppressLint("ClickableViewAccessibility")
    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setOnTouchListener(new OnSwipeTouchListener(context) {
            public void onSwipeTop() {
                System.out.println("Swipe top");
            }
            public void onSwipeRight() {
                moveRight();
            }
            public void onSwipeLeft() {
                moveLeft();
            }
            public void onSwipeBottom() { rotate(); }
        });

        SurfaceHolder surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);

        gameBoard = new GameBoard(this.getWidth(), this.getHeight(), GRIDWIDTH, GRIDHEIGHT);
        gameThread = new GameThread(surfaceHolder, this);
    }

    public void moveLeft(){
        gameBoard.moveLeft();
    }

    public void moveRight(){
        gameBoard.moveRight();
    }

    public void rotate() { gameBoard.rotate(); }

    public GameThread getGameThread() {
        return gameThread;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        gameThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        canvasWidth = width;
        canvasHeight = height;

        gameBoard.recalculateBoardTiles(width, height);

        Log.i(MainActivity.TAG, "width: " + width + ", height: " + height);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        gameThread.stopGame();
        while (retry) {
            try {
                gameThread.join();
                retry = false;
            } catch (InterruptedException e) {
                //TODO: What is the purpose of retry? Why is this exception tossed?
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        if(!gameThread.isPaused()){

            if(!gameBoard.iterate(canvas)){
                Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
                textPaint.setColor(Color.WHITE);
                textPaint.setTextSize(60);

                canvas.drawText("GAME OVER", 100, 100, textPaint);
                canvas.drawText(gameBoard.getPoints() + " " + getResources().getString(R.string.points), 100, 200, textPaint);
                gameThread.stopGame();

            }
        }

        gameBoard.drawBoard(canvas);
    }
}
