package johan.tetris.Tetrominoes;

import android.graphics.Color;

import johan.tetris.GameBoard;
import johan.tetris.Tetromino;

public class I extends Tetromino {

    private int[][][] orientationOffsets =
            {{{0, 1}, {1, 1}, {2, 1}, {3, 1}},
                    {{2, 0}, {2, 1}, {2, 2}, {2, 3}},
                    {{0, 2}, {1, 2}, {2, 2}, {3, 2}},
                    {{1, 0}, {1, 1}, {1, 2}, {1, 3}}
            };

    private GameBoard gameBoard;

    public I(int id, int orientation, int[] position, GameBoard gameBoard) {
        super(id, TetrominoType.I, orientation, position, gameBoard);

        this.gameBoard = gameBoard;
        this.setTileOffsets(orientationOffsets[orientation]);
    }

    @Override
    public int getColor() {
        return Color.rgb(97, 158, 255);
    }

    @Override
    public void rotate() {
        this.setTileOffsets(orientationOffsets[(this.getOrientation() + 1) % 4]);

        for(int[] orientation : orientationOffsets[this.getOrientation()]){
            gameBoard.clearTile(orientation[0] + this.getPosition()[0], orientation[1] + this.getPosition()[1]);
        }

        this.setOrientation(this.getOrientation() + 1);

    }
}
