package johan.tetris;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class GameActivity extends Activity {
    public static final String TAG = "Johan Tetris";

    private String finishMsg;
    private String pauseMsg;
    private String restartMsg;

    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);

        System.out.println("ONCREATE OPTIONS MENU");

        Resources res = getResources();
        finishMsg = res.getString(R.string.finish);
        pauseMsg = res.getString(R.string.pause);
        restartMsg = res.getString(R.string.resume);

        menu.add(finishMsg);
        menu.add(pauseMsg);
        menu.add(restartMsg);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getTitle().equals(finishMsg)){
            System.out.println("Finish");
            gameView.getGameThread().stopGame();
            finish();
        }
        else if (item.getTitle().equals(pauseMsg)) {
            System.out.println("Pause");
            gameView.getGameThread().setPaused(true);
        }
        else if (item.getTitle().equals(restartMsg)) {
            System.out.println("Resume");
            gameView.getGameThread().setPaused(false);
//            gameView.setFocusable(true); // make sure we get key events
        }
        return true;
    }


    protected void onPause() {
        super.onPause();
//        gameView.getThread().setPaused(true); // pause game when Activity pauses
    }
}
