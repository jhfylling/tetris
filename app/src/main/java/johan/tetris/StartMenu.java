package johan.tetris;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.Locale;


public class StartMenu extends Activity {

    private final int QUIT = 3;
    private final int CHANGEDLANGUAGE = 2;
    private final int STARTGAME = 1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_menu);
    }

    public void setEnglish(View v){
        Locale locale = getResources().getConfiguration().locale;
        locale = new Locale("en","GB");
        Configuration config = new Configuration();
        config.locale = locale;
        Resources res = getBaseContext().getResources();
        res.updateConfiguration(config, res.getDisplayMetrics());

        setResult(CHANGEDLANGUAGE);
        finish();

        System.out.println("Set language to english");
    }

    public void setNorwegian(View v){
        Locale locale = getResources().getConfiguration().locale;
        locale = new Locale("no","NO");
        Configuration config = new Configuration();
        config.locale = locale;
        Resources res = getBaseContext().getResources();
        res.updateConfiguration(config, res.getDisplayMetrics());

        setResult(CHANGEDLANGUAGE);
        finish();

        System.out.println("Set language to norwegian");
    }

    public void startGame(View v){
        setResult(STARTGAME);
        finish();
    }

    public void quitGame(View v){
        setResult(QUIT);
        finish();
    }
}
