package johan.tetris;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Tile {
    private int x;  //x-pos in dp
    private int y;  //y-pos in dp
    private int rectSize;

    private Tetromino.TetrominoType type = Tetromino.TetrominoType.NONE;
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private int tetrominoId;

    public Tile(int x, int y, int rectSize) {
        this.x = x;
        this.y = y;
        this.rectSize = rectSize;
        paint.setColor(Color.BLACK);
    }

    public void draw(Canvas c){
        int color = Color.BLACK;;
        if(type != Tetromino.TetrominoType.NONE){
            switch (type){
                case I:
                    color = Color.rgb(97, 158, 255);
                    break;
                case O:
                    color = Color.rgb(255, 237, 36);
                    break;
                case T:
                    color = Color.rgb(134, 17, 184);
                    break;
                case S:
                    color = Color.rgb(30, 184, 17);
                    break;
                case Z:
                    color = Color.rgb(219, 11, 11);
                    break;
                case J:
                    color = Color.rgb(10, 104, 255);
                    break;
                case L:
                    color = Color.rgb(219, 101, 11 );
                    break;
            }
            paint.setColor(color);
            drawRect(c);
        }
    }

    public Tetromino.TetrominoType getType() {
        return type;
    }

    public void setType(Tetromino.TetrominoType type) {
        this.type = type;
    }

    public int getTetrominoId() {
        return tetrominoId;
    }

    public void setTetrominoId(int tetrominoId) {
        this.tetrominoId = tetrominoId;
    }

    public void removeTetrominoFromTile(Canvas c){
        type = Tetromino.TetrominoType.NONE;
        paint.setColor(Color.BLACK);
        drawRect(c);
    }

    private void drawRect(Canvas c){
        c.drawRect(x, y, x + rectSize - 1, y + rectSize - 1, paint);
    }

    public void setRectSize(int rectSize) {
        this.rectSize = rectSize;
    }

    public void setPosition(int[] pos){
        this.x = pos[0];
        this.y = pos[1];
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getRectSize() {
        return rectSize;
    }

    public Paint getPaint() {
        return paint;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }
}
