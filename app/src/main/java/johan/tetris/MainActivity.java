package johan.tetris;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "Johan Tetris";

    private String finishMsg;
    private String pauseMsg;
    private String restartMsg;

    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        Intent startMenuIntent = new Intent(MainActivity.this, StartMenu.class);

        startActivityForResult(startMenuIntent, 1);



    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 1 ){
            //Start the game

//            Intent startGame = new Intent(MainActivity.this, GameActivity.class);
//            startActivity(startGame);

            setContentView(R.layout.activity_main);

            gameView = findViewById(R.id.game_view);
        } else if (resultCode == 2){
            finish();
            startActivity(getIntent());
        } else if (resultCode == 3){
            finish();
        }

    }

    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        Resources res = getResources();
        finishMsg = res.getString(R.string.finish);
        pauseMsg = res.getString(R.string.pause);
        restartMsg = res.getString(R.string.resume);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getTitle().equals(finishMsg)){
            System.out.println("Finish");
            gameView.getGameThread().stopGame();
            try {
                gameView.getGameThread().join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finish();
            startActivity(getIntent());
        }
        else if (item.getTitle().equals(pauseMsg)) {
            System.out.println("Pause");
            gameView.getGameThread().setPaused(true);
        }
        else if (item.getTitle().equals(restartMsg)) {
            System.out.println("Resume");
            gameView.getGameThread().setPaused(false);
//            gameView.setFocusable(true); // make sure we get key events
        }
        return true;
    }


    protected void onPause() {
        super.onPause();
//        gameView.getThread().setPaused(true); // pause game when Activity pauses
    }
}
