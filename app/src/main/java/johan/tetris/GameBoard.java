package johan.tetris;

import android.graphics.Canvas;
import android.widget.Toast;

import java.util.Random;

import johan.tetris.Tetrominoes.I;
import johan.tetris.Tetrominoes.O;
import johan.tetris.Tetrominoes.T;

public class GameBoard {
    private int canvasWidth;
    private int canvasHeight;

    private int boardWidth;
    private int boardHeight;

    private Tile[][] tiles;

    private Tetromino activeTetromino;
    private int activeTetrominoId;

    private int points = 0;

    private Random random;

    public GameBoard(int canvasWidth, int canvasHeight, int boardWidth, int boardHeight) {
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.boardWidth = boardWidth;
        this.boardHeight = boardHeight;

        initBoardTiles(boardWidth, boardHeight, canvasWidth / boardWidth);

        random = new Random();
        activeTetrominoId = 0;
//        activeTetromino = new I(activeTetrominoId,0, new int[] {4,0}, this);

        addNewTetromino();

    }

    public void clearTile(int x, int y){
        tiles[x][y].setType(Tetromino.TetrominoType.NONE);
        tiles[x][y].setTetrominoId(-1);
    }


    public boolean iterate(Canvas c){

        if(doesCollide(0, 1)){
            points += checkForFullRows();
            if(addNewTetromino()){
                return false;
            }
        } else {
            activeTetromino.stepDown();
        }

        placeTetrominoOnBoard(c);


        return true;

    }

    public void drawBoard(Canvas c){
        drawTiles(c);
    }

    //TODO: Add random tetromino and check for collisions
    private boolean addNewTetromino(){

        int typeSelect = random.nextInt(3);

        switch (typeSelect){
            case 0:
                activeTetromino = new I(++activeTetrominoId, random.nextInt(4), new int[] {4, 0}, this);
                break;
            case 1:
                activeTetromino = new O(++activeTetrominoId, random.nextInt(4), new int[] {4, 0}, this);
                break;
            case 2:
                activeTetromino = new T(++activeTetrominoId, random.nextInt(4), new int[] {4, 0}, this);
                break;

        }


        return doesCollide(0,0);
    }

    private int checkForFullRows(){
        int points = 0;
        for (int y = 0; y < tiles[0].length; y++) {
            boolean rowFull = true;
            for (int x = 0; x < tiles.length; x++) {
                if(tiles[x][y].getType() == Tetromino.TetrominoType.NONE){
                    rowFull = false;
                    break;
                }
            }



            if(rowFull){

                points += 10;

                //Clear full row
                for (int i = 0; i < tiles.length; i++) {
                    clearTile(i, y);
                }

                //move all rows above one step down
                for (int i = y; i > 0; i--) {
                    for (int j = 0; j < tiles.length; j++) {
//                        tiles[j][i] = tiles[j][i-1];
                        Tile aboveTile = tiles[j][i-1];
                        tiles[j][i].setType(aboveTile.getType());
                        tiles[j][i].setTetrominoId(aboveTile.getTetrominoId());
                        clearTile(j, i-1);
                    }
                }
            }

        }

        return points;
    }

    public boolean doesCollide(int xAdd, int yAdd){
        int[] tetrominoPosition = activeTetromino.getPosition();
        int[][] tetrominoOffsets = activeTetromino.getTileOffsets();

        int x = tetrominoPosition[0] + xAdd;
        int y = tetrominoPosition[1] + yAdd;



        for (int i = 0; i < tetrominoOffsets.length; i++) {
            int newX = x + tetrominoOffsets[i][0];
            int newY = y + tetrominoOffsets[i][1];

            if(newX < 0 || newX == GameView.GRIDWIDTH){
                return true;
            }

            if(newY < 0 || newY == GameView.GRIDHEIGHT){
                return true;
            }

            Tile tile = tiles[newX][newY];
            if(tile.getType() != Tetromino.TetrominoType.NONE && tile.getTetrominoId() != activeTetrominoId){
                return true;
            }
        }

        return false;
    }

    private void drawTiles(Canvas c){
        for(Tile[] row : tiles){
            for(Tile tile : row){
                if(tile.getType() != Tetromino.TetrominoType.NONE){
                    tile.draw(c);
                }
            }
        }
    }

    public void placeTetrominoOnBoard(Canvas c){
        int[] tetrominoPosition = activeTetromino.getPosition();
        int[][] tetrominoOffsets = activeTetromino.getTileOffsets();

        int x = tetrominoPosition[0];
        int y = tetrominoPosition[1];

        for (int i = 0; i < tetrominoOffsets.length; i++) {
            Tile tile = tiles[x + tetrominoOffsets[i][0]][y + tetrominoOffsets[i][1]];
            tile.setType(activeTetromino.getType());
            tile.setTetrominoId(activeTetrominoId);
//            tile.draw(c);
        }
    }

    public void initBoardTiles(int boardWidth, int boardHeight, int rectSize){

        tiles = new Tile[boardWidth][boardHeight];

        for (int i = 0; i < boardHeight; i++) {
            for (int j = 0; j < boardWidth; j++) {
                tiles[j][i] = new Tile(j * rectSize, i * rectSize, rectSize);
                tiles[j][i].setTetrominoId(-1);
            }
        }
    }

    private void setTileRectSizeAndPosition(int rectSize){
        for (int y = 0; y < tiles[0].length; y++) {
            for (int x = 0; x < tiles.length; x++) {
                tiles[x][y].setRectSize(rectSize);
                tiles[x][y].setPosition(new int[]{ x * rectSize, y * rectSize });
            }
        }
    }

    public void recalculateBoardTiles(int canvasWidth, int canvasHeight){
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;

        setTileRectSizeAndPosition(canvasWidth / boardWidth);
    }

    public void moveLeft(){
        activeTetromino.stepLeft();
    }

    public void moveRight(){
        activeTetromino.stepRight();
    }

    public void rotate(){
        activeTetromino.rotate();
    }

    public int getPoints() {
        return points;
    }
}
