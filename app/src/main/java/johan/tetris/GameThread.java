package johan.tetris;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

public class GameThread extends Thread {
    private final static int sleepInterval = 200;

    SurfaceHolder surfaceHolder;
    GameView gameView;

    private boolean isRunning;
    private boolean paused;

    public GameThread(SurfaceHolder surfaceHolder, GameView gameView) {
        this.surfaceHolder = surfaceHolder;
        this.gameView = gameView;
    }

    @Override
    public void run() {
        Canvas c;
        while (isRunning) {
            c = null;
            try {
                c = surfaceHolder.lockCanvas(null);
                synchronized (surfaceHolder) {
                    gameView.draw(c);
                }
            } finally {
                if (c != null) {
                    surfaceHolder.unlockCanvasAndPost(c);
                }
            }
            mySleep(sleepInterval);
        }
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void mySleep(int length) {
        try {
            sleep(length);
        } catch (InterruptedException e) {
            Log.i(MainActivity.TAG, "mySleep interrupted");
        }
    }

    @Override
    public synchronized void start() {
        super.start();
        isRunning = true;
    }

    public synchronized void stopGame(){
        isRunning = false;
    }

    public boolean isPaused() {
        synchronized (surfaceHolder){
            return paused;
        }
    }

    public void setPaused(boolean paused) {
        synchronized (surfaceHolder){
            this.paused = paused;
        }
    }
}
