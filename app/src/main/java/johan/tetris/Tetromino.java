package johan.tetris;
public abstract class Tetromino {

    public enum TetrominoType {
        I,
        O,
        T,
        S,
        Z,
        J,
        L,
        NONE
    }

    private int id;

    private TetrominoType type;
    private int orientation;

    private GameBoard gameBoard;

    private int[] position; //x, y position in grid
    private int[][] tileOffsets; //A list of four positions, describing the position of each tile in the Tetromino offset to the Tetromino's position

    public Tetromino(int id, TetrominoType type, int orientation, int[] position, GameBoard gameBoard) {
        this.id = id;
        this.type = type;
        this.orientation = orientation;
        this.position = position;
        this.gameBoard = gameBoard;
    }

    public abstract int getColor();

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public int[] getPosition() {
        return position;
    }

    public void setPosition(int[] position) {
        this.position = position;
    }

    public int[][] getTileOffsets() {
        return tileOffsets;
    }

    public void setTileOffsets(int[][] tileOffsets) {
        this.tileOffsets = tileOffsets;
    }

    public TetrominoType getType() {
        return type;
    }

    public void setType(TetrominoType type) {
        this.type = type;
    }

    public abstract void rotate();

    public void stepDown(){
        for (int[] pos : tileOffsets) {
            int posY = pos[1] + position[1];
            //Check boundaries
            if(posY == GameView.GRIDHEIGHT - 1){
                return;
            }
        }


        for (int[] pos : tileOffsets){
            int posX = pos[0] + position[0];
            int posY = pos[1] + position[1];

            gameBoard.clearTile(posX, posY);
        }


        position[1] = (position[1] + 1);


    }

    public void stepLeft(){
        for (int[] pos : tileOffsets) {
            int posX = pos[0] + position[0];

            //Check boundaries
            if(posX == 0){
                return;
            }
        }

        for (int[] pos : tileOffsets){
            int posX = pos[0] + position[0];
            int posY = pos[1] + position[1];

            gameBoard.clearTile(posX, posY);
        }

        position[0] = position[0] - 1;
    }

    public void stepRight(){
        for (int[] pos : tileOffsets) {
            int posX = pos[0] + position[0];

            //Check boundaries
            if(posX == GameView.GRIDWIDTH - 1){
                return;
            }
        }

        for (int[] pos : tileOffsets){
            int posX = pos[0] + position[0];
            int posY = pos[1] + position[1];

            gameBoard.clearTile(posX, posY);
        }

        position[0] = position[0] + 1;
    }
}